-- phpMyAdmin SQL Dump
-- version 4.0.10.6
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 12, 2014 at 11:03 PM
-- Server version: 5.5.40-cll
-- PHP Version: 5.4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `golden_om`
--

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE IF NOT EXISTS `company` (
  `company_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(255) NOT NULL,
  `product_price` varchar(255) NOT NULL,
  `draw_amount` double DEFAULT '0',
  `jackput_amount` double DEFAULT '0',
  PRIMARY KEY (`company_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`company_id`, `company_name`, `product_price`, `draw_amount`, `jackput_amount`) VALUES
(1, 'GUJARAT GOLD VAPI', '11', 100, 200),
(2, 'NAV RATANAM', '11', 100, 200),
(3, '', '11', 100, 200);

-- --------------------------------------------------------

--
-- Table structure for table `draw`
--

CREATE TABLE IF NOT EXISTS `draw` (
  `draw_id` int(11) NOT NULL AUTO_INCREMENT,
  `drawdatetime` datetime DEFAULT NULL,
  `win_product_id` int(11) DEFAULT NULL,
  `win_amount` double NOT NULL DEFAULT '0',
  `is_jackpot` enum('NO','YES') NOT NULL,
  `percent` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`draw_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`product_id`, `company_id`, `product_name`) VALUES
(1, 2, 'Guru'),
(2, 2, 'Som'),
(3, 2, 'Sanesvar'),
(4, 2, 'Chandra'),
(5, 2, 'Sarasvati'),
(6, 2, 'Budh'),
(7, 2, 'Mangal'),
(8, 2, 'Sani'),
(9, 2, 'Ravi'),
(10, 2, 'Bhudhesvar');

-- --------------------------------------------------------

--
-- Table structure for table `receipt_details`
--

CREATE TABLE IF NOT EXISTS `receipt_details` (
  `receipt_details_id` int(11) NOT NULL AUTO_INCREMENT,
  `receipt_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `product_price` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`receipt_details_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `receipt_master`
--

CREATE TABLE IF NOT EXISTS `receipt_master` (
  `receipt_id` int(11) NOT NULL AUTO_INCREMENT,
  `receipt_time` datetime NOT NULL,
  `receipt_ip` varchar(15) DEFAULT NULL,
  `retailer_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `draw_id` int(11) NOT NULL,
  `hash_key` varchar(8) DEFAULT NULL,
  `receipt_cancel` tinyint(4) NOT NULL DEFAULT '0',
  `cancel_time` datetime DEFAULT NULL,
  `cancel_ip` varchar(15) DEFAULT NULL,
  `receipt_scan` tinyint(4) NOT NULL DEFAULT '0',
  `user_commission` double NOT NULL DEFAULT '0',
  `win_time` datetime DEFAULT NULL,
  `win_ip` varchar(15) DEFAULT NULL,
  `scan_time` datetime DEFAULT NULL,
  PRIMARY KEY (`receipt_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

CREATE TABLE IF NOT EXISTS `transaction` (
  `transaction_id` int(11) NOT NULL AUTO_INCREMENT,
  `retailer_id` int(11) DEFAULT NULL,
  `cr_dr` varchar(255) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `receipt_id` int(11) DEFAULT NULL,
  `trans_for_id` int(11) DEFAULT NULL,
  `notes` varchar(255) DEFAULT NULL,
  `transaction_time` datetime DEFAULT NULL,
  PRIMARY KEY (`transaction_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `trans_for`
--

CREATE TABLE IF NOT EXISTS `trans_for` (
  `trans_for_id` int(11) NOT NULL AUTO_INCREMENT,
  `trans_for` varchar(255) NOT NULL,
  PRIMARY KEY (`trans_for_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `current_balance` double NOT NULL,
  `usertype` int(11) NOT NULL COMMENT '0=admin,1=retailer',
  `user_commission` double NOT NULL DEFAULT '0',
  `user_ip` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=60 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `username`, `password`, `current_balance`, `usertype`, `user_commission`, `user_ip`) VALUES
(1, 'admin', 'd58da82289939d8c4ec4f40689c2847e', 50000, 0, 0, ''),
(30, 'omtest', 'bb6aa39fe8d3c31e5cd1d6129c01acb2', 50000, 0, 5, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users_login`
--

CREATE TABLE IF NOT EXISTS `users_login` (
  `users_login_id` int(11) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `login_time` datetime NOT NULL,
  `user_ip` varchar(15) DEFAULT NULL,
  `valid_invalid` varchar(15) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
ALTER TABLE  `company` ADD  `draw_amount2` DOUBLE NULL DEFAULT  '0',
ADD  `jackput_amount2` DOUBLE NULL DEFAULT  '0';

ALTER TABLE  `draw` ADD  `win_product_id2` INT NULL AFTER  `is_jackpot` ,
ADD  `win_amount2` DOUBLE NOT NULL DEFAULT  '0' AFTER  `win_product_id2` ,
ADD  `is_jackpot2` ENUM(  'NO',  'YES' ) NOT NULL AFTER  `win_amount2`;

ALTER TABLE `draw` ADD `last_update_time` DATETIME NULL ,
ADD `last_update_ip` VARCHAR( 15 ) NULL ;
ALTER TABLE `users` ADD `is_active` INT NOT NULL DEFAULT '1' ;
ALTER TABLE  `users` ADD  `online` DATETIME NULL;

ALTER TABLE  `receipt_master` CHANGE  `hash_key`  `hash_key` VARCHAR( 13 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;
ALTER TABLE  `users` ADD  `city` VARCHAR( 15 ) NULL , ADD  `note` TEXT NULL;
