<?php
include_once('includes/basepath.php');
set_time_limit(0);
if(!isset($_SESSION['username']) && $_SESSION['usertype'] == 0){
	header('Location: index.php');
}

function make_comparer() {
    // Normalize criteria up front so that the comparer finds everything tidy
    $criteria = func_get_args();
    foreach ($criteria as $index => $criterion) {
        $criteria[$index] = is_array($criterion)
            ? array_pad($criterion, 3, null)
            : array($criterion, SORT_ASC, null);
    }

    return function($first, $second) use (&$criteria) {
        foreach ($criteria as $criterion) {
            // How will we compare this round?
            list($column, $sortOrder, $projection) = $criterion;
            $sortOrder = $sortOrder === SORT_DESC ? -1 : 1;

            // If a projection was defined project the values now
            if ($projection) {
                $lhs = call_user_func($projection, $first[$column]);
                $rhs = call_user_func($projection, $second[$column]);
            }
            else {
                $lhs = $first[$column];
                $rhs = $second[$column];
            }

            // Do the actual comparison; do not return if equal
            if ($lhs < $rhs) {
                return -1 * $sortOrder;
            }
            else if ($lhs > $rhs) {
                return 1 * $sortOrder;
            }
        }
        return 0; // tiebreakers exhausted, so $first == $second
    };
}
?>

<html>
<head>
	<link rel="stylesheet" type="text/css" href="css/<?php echo $cssLoad;?>.css" />
	<link rel="stylesheet" type="text/css" href="css/custom.css" />
	<link href="css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
	<script src="js/jquery.datetimepicker.js"></script>
  <link rel="stylesheet" href="css/jquery.datetimepicker.css" />
	<style>
		h2{ margin:0px 0px 10px 0px;}
	</style>
</head>

<body>
	<div class="top">
	<?php include_once('menu.php');?>
		<b><span style="float:right;">Hello,<?php if(isset($_SESSION['username'])) echo $_SESSION['username'];?></span></b>
		<div class="clearfix"></div><br/>
		<span style="float:right;"><a href="logout.php">Logout</a></span>
		<div class="clearfix"></div>		
	</div>	
	<div id="mainWrapper" style="text-align:center">
		<h2>Retailer Ledger</h2>
		<form method="POST" name="frm">
			Select Retailer
			<select name="retailer_id" style="width:153px;">
			<?php 		
			$qry = "SELECT * FROM users where usertype=1 ORDER BY username";
			$rs1 = mysql_query($qry) or print(mysql_error());
			while($res = mysql_fetch_array($rs1)){ ?>
				<option value="<?php echo $res['user_id']; ?>"<?php if(isset($_POST["retailer_id"]) && trim($_POST["retailer_id"]) == $res['user_id']) echo "selected='selected'"; ?>><?php echo $res['username']; ?></option>			
			<?php } ?>
			</select>
			From Date <input type="text" id="fromDate" name="fromDate" placeholder="select From date" value="<?php if(isset($_POST['fromDate'])) {echo $_POST['fromDate'];}?>" >
			To Date <input type="text" id="toDate" name="toDate" placeholder="select To date" value="<?php if(isset($_POST['toDate'])) {echo $_POST['toDate'];}?>" >
			From Time <input type="text" id="fromTime" name="fromTime" placeholder="select From time" value="<?php if(isset($_POST['fromTime'])) {echo $_POST['fromTime'];}?>" >
			To Time <input type="text" id="toTime" name="toTime" placeholder="select To Time" value="<?php if(isset($_POST['toTime'])) {echo $_POST['toTime'];}?>" >
			<input name="submit" type="submit" value="Submit"/>
		</form>
		<?php if(isset($_POST['submit'])){ ?>
		<div class="box-body table-responsive">
			<table id="example1" width="80%" align="center" border="1" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>Date</th>
						<th>IP</th>
						<th>Description</th>
						<th>BarCode</th>
						<th>Scan Time</th>
						<th>Credit</th>
						<th>Debit</th>
						<th>Balance</th>
						<th>Balance</th>
					</tr>
				</thead>
				<tbody>                  
					<?php
					$tmparray = array();
					$retailer_id = $_POST["retailer_id"];
					$balance = $nbalance = 0;
					if(isset($_POST["fromDate"]) && $_POST["fromDate"] != "") $fromDate = date("Y-m-d",strtotime($_POST["fromDate"])); else $fromDate = "";
					if(isset($_POST["toDate"]) && $_POST["toDate"] != "") $toDate = date("Y-m-d",strtotime($_POST["toDate"])); else $toDate = "";
					//$fromDate = $_POST["fromDate"];
					//$toDate = $_POST["toDate"];
					$fromTime = $_POST["fromTime"];
					$toTime = $_POST["toTime"];
					
					if($fromDate != "" || $fromTime != ""){
						
						$sSQL = "SELECT cr_dr,amount,DATE_FORMAT(transaction_time,'%Y-%m-%d %h:%i:%s') as transaction_time FROM transaction WHERE retailer_id = ".$retailer_id;
						if($fromDate != "")
							$sSQL .= " AND DATE_FORMAT(transaction_time,'%Y-%m-%d') < '".$fromDate."'";
						if($fromTime != "")
							$sSQL .= " AND DATE_FORMAT(transaction_time,'%H:%i') < '".$fromTime."'";
						$sSQL .= " ORDER BY transaction_time";
						
						$rs1 = mysql_query($sSQL);
						if(mysql_num_rows($rs1) > 0){
							while($row1 = mysql_fetch_array($rs1)){
								if(strtolower($row1["cr_dr"]) == 'credit'){
									$balance = $balance + $row1["amount"];
									$nbalance = $nbalance + $row1["amount"];
								}else{
									$balance = $balance - $row1["amount"];
									$nbalance = $nbalance - $row1["amount"];
								}
							}
						}
						
						$sSQL = "SELECT *,DATE_FORMAT(receipt_time,'%Y-%m-%d %h:%i:%s') as newreceipt_time,DATE_FORMAT(cancel_time,'%Y-%m-%d %h:%i:%s') as newcancel_time FROM receipt_master rm,draw d WHERE rm.draw_id = d.draw_id AND rm.retailer_id = ".$retailer_id;
						if($fromDate != "")
							$sSQL .= " AND DATE_FORMAT(rm.receipt_time,'%Y-%m-%d') < '".$fromDate."'";
						if($fromTime != "")
							$sSQL .= " AND DATE_FORMAT(rm.receipt_time,'%H:%i') < '".$fromTime."'";
						$sSQL .= " ORDER BY rm.receipt_time";
						
						$rs = mysql_query($sSQL);
						if(mysql_num_rows($rs) > 0){
							while($row = mysql_fetch_array($rs)){
								$amt = $winamt = $xinamt = 0;
								$sSQL = "SELECT * FROM receipt_details WHERE receipt_id = ".$row["receipt_id"];
								$rs2 = mysql_query($sSQL);
								if(mysql_num_rows($rs2) > 0){
									while($row2 = mysql_fetch_array($rs2)){
										$amt = $amt + ($row2["quantity"]*$row2["product_price"]);
										if($row["receipt_scan"] == "1" && $row["receipt_cancel"] != "1" && $row2["product_id"] == $row["win_product_id"] && $row["win_amount"] > 0){
											$winamt = $winamt + ($row2["quantity"]*$row["win_amount"]);
										}else{
											if($row["receipt_cancel"] != "1" && $row2["product_id"] == $row["win_product_id"] && $row["win_amount"] > 0)
												$xinamt = $xinamt + ($row2["quantity"]*$row["win_amount"]);
										}
									}
	
									if($amt > 0){
										$commission = ($amt * $row["user_commission"])/100;
										$balance = $balance + $commission;
										$balance = $balance - $amt;
										
										$nbalance = $nbalance + $commission;
										$nbalance = $nbalance - $amt;
									}
									
									if($winamt > 0){
										$balance = $balance + $winamt;
										$nbalance = $nbalance + $winamt;
									}
									
									if($xinamt > 0){
										$nbalance = $nbalance + $xinamt;
									}
								}
	
								if($row["receipt_cancel"] == "1" && $amt > 0){
									$commission = ($amt * $row["user_commission"])/100;
									$balance = $balance + $amt;
									$balance = $balance - $commission;
									
									$nbalance = $nbalance + $amt;
									$nbalance = $nbalance - $commission;
								}
							}
						}
					}

					$sSQL = "SELECT cr_dr,amount,DATE_FORMAT(transaction_time,'%Y-%m-%d %h:%i:%s') as transaction_time FROM transaction WHERE retailer_id = ".$retailer_id;
					if($fromDate != "")
						$sSQL .= " AND DATE_FORMAT(transaction_time,'%Y-%m-%d') >= '".$fromDate."'";
					if($toDate != "")
						$sSQL .= " AND DATE_FORMAT(transaction_time,'%Y-%m-%d') <= '".$toDate."'";
					if($fromTime != "")
						$sSQL .= " AND DATE_FORMAT(transaction_time,'%H:%i') >= '".$fromTime."'";
					if($toTime != "")
						$sSQL .= " AND DATE_FORMAT(transaction_time,'%H:%i') <= '".$toTime."'";
					$sSQL .= " ORDER BY transaction_time";
					//echo $sSQL."<br>";
					$rs1 = mysql_query($sSQL);
					if(mysql_num_rows($rs1) > 0){
						while($row1 = mysql_fetch_array($rs1)){
							if(strtolower($row1["cr_dr"]) == 'credit'){
								$txt = "Payment (Credit)";
								$credit = $row1["amount"];
								$debit = 0;
								//$balance = $balance + $row1["amount"];
							}else{
								$txt = "Payment (Debit)";
								$credit = 0;
								$debit = $row1["amount"];
								//$balance = $balance - $row1["amount"];
							}
							$tmparray[] = array("date" => $row1["transaction_time"], "detail" => $txt, "credit" => formatAmt($credit), "debit" => formatAmt($debit), "ip" => '', "bcode" => '', "nbalance" => 0,"scantime" => '');
						}
					}
				
					$sSQL = "SELECT *,DATE_FORMAT(receipt_time,'%Y-%m-%d %h:%i:%s') as newreceipt_time,DATE_FORMAT(cancel_time,'%Y-%m-%d %h:%i:%s') as newcancel_time,DATE_FORMAT(scan_time,'%Y-%m-%d %h:%i:%s') as scan_time FROM receipt_master rm,draw d WHERE rm.draw_id = d.draw_id AND rm.retailer_id = ".$retailer_id;
					if($fromDate != "")
						$sSQL .= " AND DATE_FORMAT(rm.receipt_time,'%Y-%m-%d') >= '".$fromDate."'";
					if($toDate != "")
						$sSQL .= " AND DATE_FORMAT(rm.receipt_time,'%Y-%m-%d') <= '".$toDate."'";
					if($fromTime != "")
						$sSQL .= " AND DATE_FORMAT(rm.receipt_time,'%H:%i') >= '".$fromTime."'";
					if($toTime != "")
						$sSQL .= " AND DATE_FORMAT(rm.receipt_time,'%H:%i') <= '".$toTime."'";
					$sSQL .= " ORDER BY rm.receipt_time";
					//echo $sSQL."<br>";
					$rs = mysql_query($sSQL);
					if(mysql_num_rows($rs) > 0){
						while($row = mysql_fetch_array($rs)){
							$amt = $winamt = $xinamt = 0;
							$sSQL = "SELECT * FROM receipt_details WHERE receipt_id = ".$row["receipt_id"];
							$rs2 = mysql_query($sSQL);
							if(mysql_num_rows($rs2) > 0){
								while($row2 = mysql_fetch_array($rs2)){
									$amt = $amt + ($row2["quantity"]*$row2["product_price"]);
									if($row["receipt_scan"] == "1" && $row["receipt_cancel"] != "1" && $row2["product_id"] == $row["win_product_id"] && $row["win_amount"] > 0){
										$winamt = $winamt + ($row2["quantity"]*$row["win_amount"]);
									}else{
										if($row["receipt_cancel"] != "1" && $row2["product_id"] == $row["win_product_id"] && $row["win_amount"] > 0)
											$xinamt = $xinamt + ($row2["quantity"]*$row["win_amount"]);
									}
								}

								if($amt > 0){
									$commission = ($amt * $row["user_commission"])/100;
									$tmparray[] = array("date" => $row["newreceipt_time"], "detail" => 'Ticket Buy & Commissin', "credit" => formatAmt($commission), "debit" => formatAmt($amt), "ip" => $row["receipt_ip"], "bcode" => $row["hash_key"], "nbalance" => 0,"scantime" => $row["scan_time"]);
								}
								
								if($winamt > 0){
									$tmparray[] = array("date" => $row["win_time"], "detail" => 'Ticket Win', "credit" => formatAmt($winamt), "debit" => 0, "ip" => $row["win_ip"], "bcode" => $row["hash_key"], "nbalance" => 0,"scantime" => $row["scan_time"]);
								}
								
								if($xinamt > 0){
									$tmparray[] = array("date" => $row["win_time"], "detail" => 'Ticket Win', "credit" => formatAmt($xinamt), "debit" => 0, "ip" => $row["win_ip"], "bcode" => $row["hash_key"], "nbalance" => 1,"scantime" => $row["scan_time"]);
								}
							}

							if($row["receipt_cancel"] == "1" && $amt > 0){
								$commission = ($amt * $row["user_commission"])/100;
								$tmparray[] = array("date" => $row["newcancel_time"], "detail" => 'Ticket Cancel & Commission', "credit" => formatAmt($amt), "debit" => formatAmt($commission), "ip" => $row["cancel_ip"], "bcode" => $row["hash_key"], "nbalance" => 0,"scantime" => $row["scan_time"]);
							}
						}
					}
					
					usort($tmparray, make_comparer('date'));
					//$balance = 0;
					for($i=0;$i<count($tmparray);$i++){
						if($tmparray[$i]['credit'] > 0)
							$nbalance = $nbalance + $tmparray[$i]['credit'];
						if($tmparray[$i]['debit'] > 0)
							$nbalance = $nbalance - $tmparray[$i]['debit'];
						
						if($tmparray[$i]['nbalance'] == "0"){
							if($tmparray[$i]['credit'] > 0)
								$balance = $balance + $tmparray[$i]['credit'];
							if($tmparray[$i]['debit'] > 0)
								$balance = $balance - $tmparray[$i]['debit'];
						}
						?>

						<tr <?php if($tmparray[$i]['nbalance'] == "1") echo ' style="background-color:#ccc"'; ?>>
							<td><?php echo date("d-m-Y h:i:s",strtotime($tmparray[$i]['date'])); ?></td>
							<td><?php echo $tmparray[$i]['ip']; ?></td>
							<td><?php echo $tmparray[$i]['detail']; ?></td>
							<td align="center"><?php echo $tmparray[$i]['bcode']; ?></td>
							<td><?php echo ($tmparray[$i]['scantime'] != '')?date("d-m-Y h:i:s",strtotime($tmparray[$i]['scantime'])):''; ?>&nbsp;</td>
							<td align="right"><?php echo $tmparray[$i]['credit']; ?></td>
							<td align="right"><?php echo $tmparray[$i]['debit']; ?></td>
							<td align="right"><?php echo formatAmt($balance); ?></td>
							<td align="right"><?php echo formatAmt($nbalance); ?></td>
						</tr>
						<?php
					}
					?>
				</tbody>
			</table>
		</div><!-- /.box-body -->
		<?php } ?>
	</div> 

	<script language="javascript">
    jQuery(document).ready(function() {
    	jQuery('#fromDate').datetimepicker({timepicker:false,format:'d-m-Y'});
			jQuery('#toDate').datetimepicker({timepicker:false,format:'d-m-Y'});
			jQuery('#fromTime').datetimepicker({datepicker:false,format:'H:i'});
			jQuery('#toTime').datetimepicker({datepicker:false,format:'H:i'});
    	//jQuery('#fromDate').datepicker({ dateFormat: 'dd-mm-yy' });
    	//jQuery('#toDate').datepicker({ dateFormat: 'dd-mm-yy' });
    })
	</script>
</body>
</html>