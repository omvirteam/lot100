<?php
include_once('includes/basepath.php');
if(!isset($_SESSION['user_id'])){
	header('Location: ./index.php');
	exit;
}
?>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" type="text/css" href="js/plugins/sweetalert/sweet-alert.css" />
	<link rel="stylesheet" href="css/bootstrap.min.css"/>
	<link rel="stylesheet" href="css/bootstrapreset.css"/>
	<link rel="stylesheet" href="css/mobile.css"/>

    <script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="js/plugins/sweetalert/sweet-alert.js"></script>
</head>
<body>
	<div id="centerPopup">
      <div class="closeDiv">
          <input type="button" value="X" class="close" id="closeDiv" />
      </div>
      <div id="centerPopupInner"></div>
  </div>
    <header>
        <div class="container">
            <div class="row text-center">
                <div class="col-md-4 col-xs-4 col-sm-4">
                    <div class="time-info" id="CurrentTime">
                        <p id="ctime">7:09</p>
                        <p id="ltime"></p>
                    </div>
                </div>
                <div class="col-md-4 col-xs-4 col-sm-4">
				    <div class="prcblkinfo">
                        <p class="currentBalance">
                            <?php
							//Update balance on mobile
							$select = "SELECT * FROM draw WHERE drawdatetime < '".date('Y-m-d H:i:s')."' ORDER BY drawdatetime";
  $rs = mysql_query($select);
  $winTime = date('Y-m-d H:i:s');
  $iswinner=0;
  while($row = mysql_fetch_array($rs)){
    $sSQL = "SELECT receipt_id,retailer_id FROM receipt_master where receipt_scan = 0 AND draw_id = ".$row['draw_id']." and retailer_id='".$_SESSION['user_id']."'";
    $rs2 = mysql_query($sSQL) or print(mysql_error());
    while($row2 = mysql_fetch_array($rs2)){
      $sSQL = "SELECT quantity FROM receipt_details WHERE product_id = ".$row['win_product_id']." AND receipt_id = ".$row2["receipt_id"];
      $rs3 = mysql_query($sSQL) or print(mysql_error());
      if(mysql_num_rows($rs3) > 0){
        $row3 = mysql_fetch_array($rs3);
        mysql_query("UPDATE users SET current_balance = current_balance + ".($row3["quantity"]*$row['win_amount'])." WHERE user_id = ".$row2["retailer_id"]);
		$iswinner=1;
      }
      mysql_query("UPDATE receipt_master SET receipt_scan = 1,scan_time='".$winTime."' WHERE receipt_id = ".$row2["receipt_id"]);
    
      if($allow_twowin && !is_null($row['win_product_id2'])){
        $sSQL = "SELECT quantity FROM receipt_details WHERE product_id = ".$row['win_product_id2']." AND receipt_id = ".$row2["receipt_id"];
        $rs3 = mysql_query($sSQL) or print(mysql_error());
        if(mysql_num_rows($rs3) > 0){
          $row3 = mysql_fetch_array($rs3);
          mysql_query("UPDATE users SET current_balance = current_balance + ".($row3["quantity"]*$row['win_amount2'])." WHERE user_id = ".$row2["retailer_id"]);
		  $iswinner=1;
        }
      }
	  
	  mysql_query("UPDATE receipt_master SET win_time='".$winTime."',win_ip='".$_SERVER['REMOTE_ADDR']."' WHERE receipt_id = ".$row2["receipt_id"]);
    }
  } 
							
							
							$sSQL = "SELECT current_balance FROM users WHERE user_id = ".$_SESSION['user_id'];
							$rs = mysql_query($sSQL) or print(mysql_error());
							$row = mysql_fetch_array($rs);
							echo round($row["current_balance"]);
							?>
                        </p>
                        <p class="pnno"><?php echo rtnretailer($_SESSION['user_id']); ?></p>                            
				    </div>                    
                </div>
                <div class="col-md-4 col-xs-4 col-sm-4">
				    <p id="currentTimer" class="curTime">02:14:12</p>
				    <p class="dtcls"><?php echo date('d/m/Y');?></p>                        
                </div>
            </div>
        </div>
    </header>
    <section class="contants">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div id="yantra_list" class="clearfix">
                        <form id="receipt" role="form">
                            <ul class="yantra_list clearfix">	
                                <li><img src="./images/<?php echo $imageLoad;?>GW01.jpg"><div class="form-group"><input name="1" class="udlrClass onlynum form-control" dataindex="1" id="1" type="text"></div></li>
                                <li><img src="./images/<?php echo $imageLoad;?>GW02.jpg"><div class="form-group"><input name="2" class="udlrClass onlynum form-control" dataindex="2" id="2" type="text"></div></li>
                                <li><img src="./images/<?php echo $imageLoad;?>GW03.jpg"><div class="form-group"><input name="3" class="udlrClass onlynum form-control" dataindex="3" id="3" type="text"></div></li>
                                <li><img src="./images/<?php echo $imageLoad;?>GW04.jpg"><div class="form-group"><input name="4" class="udlrClass onlynum form-control" dataindex="4" id="4" type="text"></div></li>
                                <li><img src="./images/<?php echo $imageLoad;?>GW05.jpg"><div class="form-group"><input name="5" class="udlrClass onlynum form-control" dataindex="5" id="5" type="text"></div></li>
                                <li><img src="./images/<?php echo $imageLoad;?>GW06.jpg"><div class="form-group"><input name="6" class="udlrClass onlynum form-control" dataindex="6" id="6" type="text"></div></li>
                                <li><img src="./images/<?php echo $imageLoad;?>GW07.jpg"><div class="form-group"><input name="7" class="udlrClass onlynum form-control" dataindex="7" id="7" type="text"></div></li>
                                <li><img src="./images/<?php echo $imageLoad;?>GW08.jpg"><div class="form-group"><input name="8" class="udlrClass onlynum form-control" dataindex="8" id="8" type="text"></div></li>
                                <li><img src="./images/<?php echo $imageLoad;?>GW09.jpg"><div class="form-group"><input name="9" class="udlrClass onlynum form-control" dataindex="9" id="9" type="text"></div></li>
                                <li><img src="./images/<?php echo $imageLoad;?>GW10.jpg"><div class="form-group"><input name="10" class="udlrClass onlynum form-control" dataindex="10" id="10" type="text"></div></li>
                                
                            <input name="draw_id" id="draw_id" value="1392" type="hidden">
                            <input style="display:none;" name="submit" value="Submit" type="submit">
                            
                        </form>
                        
						<article class="c-btn clearfix">
							<div class="g-left">
								<div class="form-group"><input name="qty" id="qty" class="test1 form-control" readonly type="text"></div>
								<div class="form-group"><input name="amt" id="amt" class="test2 form-control" readonly type="text"></div>
							</div>
							<div class="g-right">
								<input name="buy" id="buy" value="Buy" class="btn btn-warning" type="button">
                            </div>
						</article>
                        
                    </div>
                    <article class="extra-btn clearfix">
                        <input name="lastresult" id="lastresult" value="Last Result" class="btn btn-warning pull-left" type="button">
                        <!--<input name="exit" value="View Transaction" class="btn btn-primary pull-left" type="button">-->
                        <input name="exit" value="Exit" class="btn btn-danger pull-right exit" type="button" >
                        
                    </article>
                </div>
                <div class="col-md-12">
                    <div id="footer" class="footer">
  	<?php
  	$sSQL = "SELECT	product_price FROM company WHERE company_id = ".$company_id;
  	$rs = mysql_query($sSQL);
  	$product_price = 0;
  	if(mysql_num_rows($rs) > 0){
  		$row = mysql_fetch_array($rs);
  		$product_price = $row["product_price"];
  	}
  	?>
  	<input type="hidden" name="product_price" id="product_price" value="<?php echo $product_price;?>" />
                        <div class="row">
                            <!--<div class="col-xs-4 col-sm-12">
                                <input name="drawlist" value="List Draw" class="btn btn-primary" type="button">
                            </div>-->
                            <div class="col-xs-6 col-sm-12">
                                <input name="current" value="Current" class="btn btn-primary" onClick="getCurrentData(0)" type="button">
                            </div>
                            <div class="col-xs-6 col-sm-12">
                                <input name="upcoming" value="Upcoming" class="btn btn-info" onClick="UpComing()" type="button">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3 col-sm-12">
                                <input name="clear" id="clear" value="Clear" class="btn btn-success" type="button">
                            </div>
                            <div class="col-xs-3 col-sm-12">
                                <input name="can" id="can" value="Can. Rec." class="btn btn-success" type="button">
                            </div>
                            <div class="col-xs-3 col-sm-12">
                                <input name="purchase" value="Pur. Det." class="btn btn-info pdetail" type="button">
                            </div>
                            <div class="col-xs-3 col-sm-12">
                                <input name="luckyyantra" value="Lucky Yantras" class="btn btn-info luckyyantra" type="button">
                            </div>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
    </section>
	<section class="y-content">
	    <div class="container">
	        <div class="row">
	            <div class="col-md-12">
			<div id="content"></div>
		    </div>
		</div>
	    </div>
	</section>
    <input type="hidden" name="nextdrawid" id="nextdrawid" value="" />
                        <input type="hidden" name="nextdrawtime" id="nextdrawtime" value="" />
                        <input type="hidden" name="currenttime" id="currenttime" value="" /> 
<div id="iframeDiv" style="display:none"></div>
  <script type="text/javascript">
  	imageLoad = "<?php echo $imageLoad; ?>";
  	allow_twowin = "<?php echo $allow_twowin; ?>";
  	cpackage = "<?php echo $package; ?>";
  </script>
  <script type="text/javascript" src="js/mobilecustom.js"></script>
</body>
</html>