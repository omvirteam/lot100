<?php
include_once('includes/basepath.php');
if(!isset($_SESSION['user_id'])){
	header('Location: ./index.php');
	exit;
}
?>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" type="text/css" href="js/plugins/sweetalert/sweet-alert.css" />
	<link rel="stylesheet" href="css/bootstrap.min.css"/>
	<link rel="stylesheet" href="css/bootstrapreset.css"/>
	<link rel="stylesheet" href="css/mobile.css"/>

    <script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="js/plugins/sweetalert/sweet-alert.js"></script>
</head>
<body>
	<div id="centerPopup">
      <div class="closeDiv">
          <input type="button" value="X" class="close" id="closeDiv" />
      </div>
      <div id="centerPopupInner"></div>
  </div>
    <header>
        <div class="container">
            <div class="row text-center">
                <div class="col-md-4 col-xs-4 col-sm-4">
                    <div class="time-info" id="CurrentTime">
                        <p id="ctime">7:09</p>
                        <p id="ltime"></p>
                    </div>
                </div>
                <div class="col-md-4 col-xs-4 col-sm-4">
				    <div class="prcblkinfo">
                        <p class="currentBalance">
                            <?php
							$sSQL = "SELECT current_balance FROM users WHERE user_id = ".$_SESSION['user_id'];
							$rs = mysql_query($sSQL) or print(mysql_error());
							$row = mysql_fetch_array($rs);
							echo round($row["current_balance"]);
							?>
                        </p>
                        <p class="pnno"><?php echo rtnretailer($_SESSION['user_id']); ?></p>                            
				    </div>                    
                </div>
                <div class="col-md-4 col-xs-4 col-sm-4">
				    <p id="currentTimer" class="curTime">02:14:12</p>
				    <p class="dtcls"><?php echo date('d/m/Y');?></p>                        
                </div>
            </div>
        </div>
    </header>
    <section class="contants">
	<div class="container">
            <div class="row">
                <div class="col-md-12">
					<div id="yantra_list" class="clearfix">
	<?php
	if(isset($_POST['frmdate']) && isset($_POST['todate'])) {
		$frmdate = date('Y-m-d', strtotime($_POST['frmdate']));
		$todate = date('Y-m-d', strtotime($_POST['todate']));
		$sale = $saleQty = $purchase = $silver = $silverQty = $cancel = $cancelQty = $commision = 0;
		$sSQL = "SELECT receipt_id,user_commission,d.win_product_id,d.win_amount,d.win_product_id2,d.win_amount2 FROM receipt_master rm,draw d WHERE (DATE_FORMAT(drawdatetime,'%Y-%m-%d') BETWEEN '".$frmdate."' AND '".$todate."') AND rm.draw_id = d.draw_id AND receipt_cancel = 0 AND retailer_id = ".$_SESSION['user_id'];
		$rs = mysql_query($sSQL);
		if(mysql_num_rows($rs) > 0) {
			while($row = mysql_fetch_assoc($rs)) {
				$sSQL = "SELECT quantity,product_price,product_id FROM receipt_details WHERE receipt_id = '".$row["receipt_id"]."'";
				$rs1 = mysql_query($sSQL);
				while($row1 = mysql_fetch_array($rs1)){
					$tmpamt = ($row1["quantity"] * $row1["product_price"]);					
					$sale = $sale + $tmpamt;
					$saleQty = $saleQty + $row1["quantity"];
					$purchase = ($purchase + $tmpamt) - ($tmpamt*$row["user_commission"])/100;
					$commision = $commision + ($tmpamt*$row["user_commission"])/100;
					if($row1["product_id"] == $row["win_product_id"] && $row["win_amount"] > 0){
						$silverQty = $silverQty + $row1["quantity"];
						$silver = $silver + ($row1["quantity"] * $row["win_amount"]);
					}
					if($allow_twowin):
						if($row1["product_id"] == $row["win_product_id2"] && $row["win_amount2"] > 0){
							$silverQty = $silverQty + $row1["quantity"];
							$silver = $silver + ($row1["quantity"] * $row["win_amount2"]);
						}
					endif;
				}
			}
		}
		
		if($sale > 0){
		?>
		<div class="mainwrapper" >		
			<?php if($package == 2): ?>
			<?php
			$Recharge = 0;
			$sSQL = "SELECT * FROM transaction WHERE (DATE_FORMAT(transaction_time,'%Y-%m-%d') BETWEEN '".$frmdate."' AND '".$todate."') AND retailer_id = ".$_SESSION['user_id'];
			$rs = mysql_query($sSQL);
			if(mysql_num_rows($rs) > 0) {
				while($row = mysql_fetch_assoc($rs)){
					if($row["cr_dr"] == 'Credit')
						$Recharge = $Recharge + $row["amount"];
					else
						$Recharge = $Recharge - $row["amount"];
				}
			}
			
			$qry = "SELECT company_name FROM company WHERE company_id = '$company_id'";
			$resu= mysql_query($qry) or print(mysql_error());
			$rescomp = mysql_fetch_assoc($resu);
			
			$qry = "SELECT current_balance FROM users WHERE user_id = ".$_SESSION['user_id'];
			$rsuser= mysql_query($qry) or print(mysql_error());
			$resuser = mysql_fetch_array($rsuser);
			?>
			<div class="subwrapper" style="border-bottom:dotted 2px #514F4F;"><b><?php echo $rescomp['company_name']; ?></b></div>
			<div class="subdate"><b><span><?php echo date("d/m/Y h:i A");?> C.id: <?php echo ucfirst(rtnretailer($_SESSION['user_id']));?></span></b></div>
			<br />
			<table width="15%" cellspacing="0" cellpadding="1" id="tableList">
					<tr>
						<td style="border-bottom:dotted 2px #514F4F;"><b>Total Sales</b></td>
						<td style="border-bottom:dotted 2px #514F4F;" align="right"><?php echo formatAmt($sale); ?></td>
					</tr>
					<tr>
						<td style="border-bottom:dotted 2px #514F4F;"><b>Total Clame</b></td>
						<td style="border-bottom:dotted 2px #514F4F;" align="right"><?php echo formatAmt($silver); ?></td>
					</tr>
					<tr>
						<td style="border-bottom:dotted 2px #514F4F;"><b>Recharge</b></td>
						<td style="border-bottom:dotted 2px #514F4F;" align="right"><?php echo formatAmt($Recharge); ?></td>
					</tr>
					<tr>
						<td style="border-bottom:dotted 2px #514F4F;"><b>Total Commission</b></td>
						<td style="border-bottom:dotted 2px #514F4F;" align="right"><?php echo formatAmt($commision); ?></td>
					</tr>
					<tr>
						<td style="border-bottom:dotted 2px #514F4F;"><b>Balance</b></td>
						<td style="border-bottom:dotted 2px #514F4F;" align="right"><?php echo formatAmt($resuser['current_balance']); ?></td>
					</tr>
			</table>
			<?php else: ?>
			<div class="subwrapper" style="border-bottom:dotted 2px #514F4F;clear:both;">
				<b>Purchase Receipt</b>
			</div>
			<div class="subdate">
				<b><span>From Date: <?php echo date("d/m/Y", strtotime($_POST['frmdate']));?><br />To Date: <?php echo date("d/m/Y", strtotime($_POST['todate']));?> </span></b>
			</div>
			
			<table width="100%" cellspacing="0" cellpadding="0" class="table">
					<tr>
						<td style="border-bottom:dotted 2px #514F4F;">&nbsp;</td>
						<td align="right" style="border-bottom:dotted 2px #514F4F;"><b>Amount</b></td>
						<td align="right" style="border-bottom:dotted 2px #514F4F;padding-right:10px;"><b>Qty</b></td>
					</tr>
					<tr>
						<td><b>Sale</b></td>
						<td align="right"><?php echo formatAmt($sale); ?></td>
						<td align="right" style="padding-right:10px;"><?php echo $saleQty; ?></td>
					</tr>
					<tr>
						<td><b>Purchase</b></td>
						<td align="right"><?php echo formatAmt($purchase); ?></td>
						<td align="right" style="padding-right:10px;"><?php echo $saleQty; ?></td>
					</tr>
					<tr>
						<td><b>Silver Points<br />(Worth Rs.)</b></td>
						<td align="right"><?php echo formatAmt($silver); ?></td>
						<td align="right" style="padding-right:10px;"><?php echo $silverQty; ?></td>
					</tr>
					<tr>
						<td><b>Commission</b></td>
						<td align="right"><?php echo formatAmt($sale-$purchase); ?></td>
						<td align="right" style="padding-right:10px;">&nbsp;</td>
					</tr>
					<tr>
						<td style="border-bottom:dotted 2px #514F4F;"><b>Bal</b></td>
						<td align="right" style="border-bottom:dotted 2px #514F4F;"><?php echo formatAmt($purchase-$silver); ?></td>
						<td align="right" style="border-bottom:dotted 2px #514F4F;padding-right:10px;">&nbsp;</td>
					</tr>
			</table>
			<?php endif; ?>
		</div>
		<hr style="border:solid 1px #000;">
		<script type="text/javascript">
		//javascript:window.print();
		//setTimeout("closePrintView()", 10000); //delay required for IE to realise what's going on
		//window.onafterprint = closePrintView(); //this is the thing that makes it work i
		function closePrintView() { //this function simply runs something you want it to do
		  //document.location.href = "dashboard.php"; //in this instance, I'm doing a re-direct
		}
		</script>
		<div class="pagebreak"></div>
		<?php } else { echo "No records found."; ?>
			<script type="text/javascript">
				//window.location.replace('dashboard.php');
			</script>
			<?php
		}
	} else { ?>
		<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
		<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" /> 
	    <script type="text/javascript" src="js/jquery-ui.js"></script>
		<div class="formwrapper">
			<form method="post" action="mpurchase.php" id="showtimeform">
				<table width="100%" cellspacing="0" cellpadding="0" class="table responsive">
					<tr>
						<td>From Date:</td>
						<td>
						 <input type="text" id="frmdate" name="frmdate" placeholder="Select Date" value="<?php if (isset($_POST['frmdate'])) { echo $_POST['frmdate']; } ?>">
						</td>
					</tr>
					<tr>
						<td>To Date:</td>
						<td><input type="text" id="todate" name="todate" placeholder="Select Date" value="<?php if (isset($_POST['todate'])) { echo $_POST['todate']; } ?>">
						</td>
					</tr>
					<tr>
						<td></td>
						<td align="left">
							<input class="btn btn-primary" type="button" name="showtime" value="Print" id="printpurchase">
						</td>
					</tr>
				</table>
			</div>
			<script language="javascript">
			jQuery(document).ready(function() {
				jQuery('#frmdate').datepicker({ dateFormat: 'dd-mm-yy' });
				jQuery('#todate').datepicker({ dateFormat: 'dd-mm-yy' });
				
				jQuery('#printpurchase').on('click', function() {
					if (jQuery('#frmdate').val() != '' && jQuery('#todate').val() != '') {
						jQuery('#showtimeform').submit();
					}
					else {
						alert("Please select dates");
						return false;
					}
				});
			});
			</script>
		<?php
	}
	?>
	<table width="100%" cellspacing="0" cellpadding="0" class="table table-responsive">
		<tr>
			<td align="center">
				<input class="btn btn-primary" type="button" name="back" value="Back" id="back">
			</td>
		</tr>
	</table>
	
	<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery('#back').on('click', function() {
			window.location.replace('mdashboard.php');
		});
	});

	</script>
</body>
</html>