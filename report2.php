<?php
include_once('includes/basepath.php');if(!isset($_SESSION['username'])){	header('Location: index.php');}

$recordsForDate = isset($_POST['date']) ? substr($_POST['date'],0,4)."-".substr($_POST['date'],5,2)."-".substr($_POST['date'],8,2) : date("Y-m-d");
?>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="css/<?php echo $cssLoad;?>.css" />
	<link rel="stylesheet" type="text/css" href="css/custom.css" />
  <link rel="stylesheet" href="css/jquery-ui.css" />
	<link href="css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
	<script src="js/jquery-ui.js"></script>
	<script type="text/javascript" src="js/cycle.js"></script>
	<script type="text/javascript" src="js/cycle.tile.js"></script>
	<!-- DATA TABES SCRIPT -->
	<script src="js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
	<script src="js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>	
  <script language="javascript">
    jQuery(document).ready(function() {
        jQuery('#date').datepicker({ dateFormat: 'yy-mm-dd' });
        //.datepicker("setDate", new Date())
        $('#auto-branch').autocomplete({
			    source: [ <?php
			        $addFromComma = "";
			        $userRs = "SELECT user_id,username FROM users ORDER BY username";
			        $selectUser = mysql_query($userRs);
			        while($selectUserData = mysql_fetch_assoc($selectUser)){
			            echo $addFromComma . '"' . $selectUserData['username'] . '"';
			            $addFromComma = ",";
			        }
			        ?>
			    ]
		    });
		});
  </script>
</head>

<body>
	<?php include_once('menu.php');?>
	<br />
	<br />
  <form name="frm" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
    <input type="text" id="date" name="date" placeholder="Select Date" 
           value="<?php  echo $recordsForDate;  ?>">
		<?php
    $username = "";
    if(isset($_POST["username"]) && trim($_POST["username"])){
    	$username = $_POST["username"];
	  }
    ?>
		Select Retailer : 
		<input id="auto-branch" type="text" name="username" value="<?php echo $username; ?>">
		<!--<select name="user_id" onchange="frm.submit();">
			<option value="0">All</option>
			<?php
			$sSQL = "SELECT user_id,username FROM users ORDER BY username"; //WHERE usertype = 1
			$rs = mysql_query($sSQL);
			if(mysql_num_rows($rs) > 0){
			  $userCount = 0;
				while($row = mysql_fetch_array($rs)){
				  $userArray[$userCount]['user_id'] = $row["user_id"];
				  $userArray[$userCount]['username'] = $row["username"];
				  
					if(isset($_POST["user_id"]) && $row["user_id"] == $_POST["user_id"]) $a = ' selected'; else $a = '';
					echo '<option value="'.$row["user_id"].'" '.$a.'>'.$row["username"].'</option>';
					
					$userCount++;
				}
			}
			?>
		</select>-->
		<?php
			$sSQL = "SELECT user_id,username FROM users ORDER BY username"; //WHERE usertype = 1
			$rs = mysql_query($sSQL);
			if(mysql_num_rows($rs) > 0){
			  $userCount = 0;
				while($row = mysql_fetch_array($rs)){
				  $userArray[$userCount]['user_id'] = $row["user_id"];
				  $userArray[$userCount]['username'] = $row["username"];
					$userCount++;
				}
			}
			?>
    <input type="submit" name="submitBtn" value="Display !">
	</form>	
	
  <table cellpadding="3" cellspacing="0" border="1" width="80%" align="center">
<?php	
	$arrOfQuery1 = $arrofQuery2 = array();
	echo "<tr>";
	echo "<td width='15%'>Time</td>";
	echo "<td width='10%'>Retailer</td>";
	echo "<td width='10%'>Total Win</td>";
	echo "<td width='10%'>Scan</td>";
	echo "<td width='10%'>NotScan</td>";
	echo "<td width='45%'>NotScan BarCode</td>";
	echo "</tr>";
  if(isset($_POST["date"])){
    $qry = "SELECT draw_id,drawdatetime,ifnull(win_product_id,-1) win_product_id,win_amount,	is_jackpot,ifnull(win_product_id2,-1) win_product_id2,win_amount2,is_jackpot2,	percent,last_update_time,last_update_ip FROM draw
            WHERE drawdatetime LIKE '".$recordsForDate."%'
            ORDER BY drawdatetime";
    $res = mysql_query($qry) or print(mysql_error());											
    $nums = mysql_num_rows($res);
    $FTotalWin = $FTotalScan = $FTotalNotScan = 0;
    while($row = mysql_fetch_array($res))
    {
       //echo "<br>Draw ) - ".$row['draw_id'];
      for($i = 0;$i < $userCount; $i++)
      {
        if(isset($_REQUEST['username']))
        {
          if($_REQUEST['username'] == '')
            $displayMe = true;
          else if($_REQUEST['username'] == $userArray[$i]['username'])
            $displayMe = true;
          else
            $displayMe = false;
        }
        else
          $displayMe = true;

        if($displayMe){
          if(($row['win_product_id']>=0 && $row['win_product_id']<=99)  
          || ($row['win_product_id2']>=0 &&$row['win_product_id2']<=99)){
             echo "<tr>";
              echo "<td>".$row['drawdatetime']."</td>";
              echo "<td NOWRAP>".$userArray[$i]['username']."</td>";

              $TotalWin = $TotalScan = $TotalNotScan = 0;

              $sSQL = "SELECT * FROM receipt_master where receipt_cancel = 0 AND draw_id = ".$row['draw_id']." AND retailer_id = ".$userArray[$i]['user_id'];
              $rs = mysql_query($sSQL);
              while($row1 = mysql_fetch_array($rs)){
                $countQuery = "SELECT SUM(quantity) AS totalWinQty from receipt_details where receipt_id = ".$row1["receipt_id"]." AND product_id = ".$row['win_product_id'];					  	$countResult = mysql_query($countQuery);
                $countRow    = mysql_fetch_array($countResult);
                if(!is_null($countRow['totalWinQty'])) $tWin = $countRow['totalWinQty']; else $tWin = 0;
                //echo "<br>Win_Amount ) - ".$tWin;
                $TotalWin = $TotalWin + ($tWin*$row['win_amount']);

                if($allow_twowin && $row['win_product_id2'] >= 0):
                  $countQuery = "SELECT SUM(quantity) AS totalWinQty from receipt_details where receipt_id = ".$row1["receipt_id"]." AND product_id = ".$row['win_product_id2'];
                  $countResult = mysql_query($countQuery);
                  $countRow    = mysql_fetch_array($countResult);
                  if(!is_null($countRow['totalWinQty'])) $tWin = $countRow['totalWinQty']; else $tWin = 0;
                  //echo "<br>Win_amount 2) - ".$tWin;
                  $TotalWin = $TotalWin + ($tWin*$row['win_amount2']);
                endif;
              }

              $sSQL = "SELECT * FROM receipt_master where receipt_cancel = 0 AND scan_time IS NULL AND draw_id = ".$row['draw_id']." AND retailer_id = ".$userArray[$i]['user_id'];
              $rs = mysql_query($sSQL);
              while($row1 = mysql_fetch_array($rs)){
                $countQuery = "SELECT SUM(quantity) AS totalWinQty from receipt_details where receipt_id = ".$row1["receipt_id"]." AND product_id = ".$row['win_product_id'];
                $countResult = mysql_query($countQuery);
                $countRow    = mysql_fetch_array($countResult);
                if(!is_null($countRow['totalWinQty'])) $tWin = $countRow['totalWinQty']; else $tWin = 0;
                //echo "<br>Not Scan ) - ".$tWin;
                $TotalScan = $TotalScan + ($tWin*$row['win_amount']);

                if($allow_twowin && $row['win_product_id2'] >= 0):
                  $countQuery = "SELECT SUM(quantity) AS totalWinQty from receipt_details where receipt_id = ".$row1["receipt_id"]." AND product_id = ".$row['win_product_id2'];
                  $countResult = mysql_query($countQuery);
                  $countRow    = mysql_fetch_array($countResult);
                  if(!is_null($countRow['totalWinQty'])) $tWin = $countRow['totalWinQty']; else $tWin = 0;
                  //echo "<br>Not Scan 2) - ".$tWin;
                  $TotalScan = $TotalScan + ($tWin*$row['win_amount2']);
                endif;
              }

              echo "<td align='right'>".$TotalWin."</td>";
              echo "<td align='right'>".($TotalWin-$TotalScan)."</td>";
              echo "<td align='right'>".$TotalScan."</td>";

              $FTotalWin = $FTotalWin + $TotalWin;
              $FTotalScan = $FTotalScan + $TotalScan; 
              $FTotalNotScan = $FTotalNotScan + ($TotalWin-$TotalScan);

              $barCode = $addComma = ''; $lastCode = "";
              $sSQL = "SELECT hash_key,receipt_details.* FROM receipt_master LEFT JOIN 	receipt_details ON receipt_master.receipt_id = receipt_details.receipt_id WHERE receipt_master.draw_id = ".$row['draw_id']." AND receipt_master.retailer_id = ".$userArray[$i]['user_id']." AND receipt_master.receipt_cancel = 0 AND receipt_master.receipt_scan = 0 AND receipt_details.product_id = ".$row['win_product_id'].' AND receipt_details.quantity>0' ;
              $rs1 = mysql_query($sSQL);
              while($row1 = mysql_fetch_array($rs1)){
                if($lastCode != $row1["hash_key"])
                  $barCode .= $addComma . "Code: " . $row1["hash_key"] . ", ";
                $k = 0;
                if($row['win_product_id'] == $row1["product_id"]){
                  $barCode .= "Qty: " . $row1["quantity"] . ", ";
                  $barCode .= "Pts: " . $row1["quantity"] * $row['win_amount']. ", ";
                  $k = 1;
                }

                if($allow_twowin && $row['win_product_id2'] >= 0):
                  $sSQL = "SELECT hash_key,receipt_details.* FROM receipt_master LEFT JOIN 	receipt_details ON receipt_master.receipt_id = receipt_details.receipt_id WHERE receipt_master.draw_id = ".$row['draw_id']." AND receipt_master.retailer_id = ".$userArray[$i]['user_id']." AND receipt_master.receipt_cancel = 0 AND receipt_master.receipt_scan = 0 AND receipt_master.hash_key = '".$row1["hash_key"]."' AND receipt_details.product_id = ".$row['win_product_id2'].' AND receipt_details.quantity>0';
                  $rs2 = mysql_query($sSQL);
                  while($row2 = mysql_fetch_array($rs2)){
                    $k = 0;
                    if($row['win_product_id2'] == $row2["product_id"]){
                      $barCode .= "Qty: " . $row2["quantity"] . ", ";
                      $barCode .= "Pts: " . $row2["quantity"] * $row['win_amount2']. ", ";
                      $k = 1;
                    }
                  }
                endif;		          

                if($lastCode != $row1["hash_key"])
                  $addComma = "<br>";
                $lastCode = $row1["hash_key"];
              }

              if($barCode == '') $barCode = "&nbsp;";
              echo "<td align='left'>".$barCode."</td>";
            echo "</tr>";
          }
        }
      }

    }	
    echo "<tr>";
		echo "<td colspan='2' align='right'><b>Total</b></td>";
		echo "<td align='right'><b>".$FTotalWin."</b></td>";
		echo "<td align='right'><b>".$FTotalNotScan."</b></td>";
		echo "<td align='right'><b>".$FTotalScan."</b></td>";
		echo "<td align='right'>&nbsp;</td>";
		echo "</tr>";							
  }
?>
  </table>

</body>
</html>