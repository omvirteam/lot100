<?php
include_once('includes/basepath.php');
if(!isset($_SESSION['username'])){
	header('Location: ./index.php');
	exit;
}
if(isset($_REQUEST["id"])) $receipt_id = $_REQUEST["id"]; else $receipt_id = 0;
if(!isset($_REQUEST["lastrec"])){
	$sSQL = "SELECT company_id,draw_id,hash_key,receipt_time,retailer_id FROM receipt_master WHERE receipt_id = ".$receipt_id;
	$rs = mysql_query($sSQL) or print(mysql_error());
	$row = mysql_fetch_array($rs);

}else{
	$sSQL = "SELECT company_id,draw_id,hash_key,receipt_time,retailer_id,receipt_id FROM receipt_master WHERE company_id = ".$company_id." AND retailer_id = ".$_SESSION['user_id']." AND receipt_time < '".date("Y-m-d H:i:s")."' ORDER BY receipt_id DESC LIMIT 0,1";
	$rs = mysql_query($sSQL) or print(mysql_error());
	$row = mysql_fetch_array($rs);
	$receipt_id = $row["receipt_id"];
}

//get company details
$cid = $row['company_id'];
$qry = "SELECT company_name FROM company WHERE company_id = '$cid'";
$resu= mysql_query($qry) or print(mysql_error());
$rescomp = mysql_fetch_array($resu);

//get draw details
$drawid = $row['draw_id'];
$drqry = "SELECT drawdatetime FROM draw WHERE draw_id = '$drawid'";
$drres= mysql_query($drqry) or print(mysql_error());
$resdraw = mysql_fetch_array($drres);

//$row['company_id'];
$sSQL = "SELECT product_id,quantity,product_price FROM receipt_details WHERE receipt_id = ".$receipt_id;
$rs1 = mysql_query($sSQL) or print(mysql_error());

?>
<html>
<head>
	<style>
		@media print { .pagebreak { page-break-after:always; } }
		<?php if($package == 3){ ?>
		body { margin:0px; padding:0px; font-size:9px; font-weight:normal; text-transform: uppercase; font-family:Verdana, Geneva, sans-serif;}
		<?php }else{ ?>
		body { margin:0px; padding:0px; font-size:9px; font-weight:bold; text-transform: uppercase; font-family:Verdana, Geneva, sans-serif;}
		<?php } ?>
	</style>
</head>
<body>
	<?php 
	$bs = "<b>";
	$be = "</b>";
	if($package == 3){ 
		$bs = "";
		$be = "";
	}
	?>
	<table style="margin-left:15px" cellspacing="0">		
		<?php if($package == 3){ ?>
        	<tr ><td align="center" colspan="4" style="border-bottom:1px solid #514F4F;font-size:9px"><?php echo $bs; ?>Acknowledgement Receipt - <?php echo $receipt_id?><?php echo $be; ?><br /></td></tr>
			<tr ><td  colspan="4" style="border-bottom:1px solid #514F4F;font-size:9px"><?php echo $bs; ?><?php echo $rescomp['company_name']?><?php echo $be; ?> <?php echo date("d/m/Y",strtotime($resdraw['drawdatetime']));?> <?php echo date("h:i A",strtotime($resdraw['drawdatetime']));?><br />Date: <?php echo date("d/m/Y h:i:s A",strtotime($row['receipt_time']));?> C.ID - <?php echo rtnretailer($row['retailer_id']);?></td></tr>
		
		<?php }else{ ?>
			<tr ><td align="center" colspan="4" style="border-bottom:1px solid #514F4F;font-size:9px"><?php echo $bs; ?>Acknowledgement Receipt - <?php echo $receipt_id?><?php echo $be; ?><br /></td></tr>
			<tr ><td  colspan="4" style="border-bottom:1px solid #514F4F;font-size:9px"><?php echo $bs; ?><?php echo $rescomp['company_name']?><?php echo $be; ?> <?php echo date("d/m/Y",strtotime($resdraw['drawdatetime']));?> <?php echo date("h:i A",strtotime($resdraw['drawdatetime']));?><br />Date: <?php echo date("d/m/Y h:i:s A",strtotime($row['receipt_time']));?> C.ID - <?php echo rtnretailer($row['retailer_id']);?></td></tr>
			
		<?php } ?>
		<tr ><td style="border-bottom:1px solid #514F4F;font-size:9px">PCode Qty</td><td style="border-bottom:1px solid #514F4F;font-size:9px">PCode Qty</td><td style="border-bottom:1px solid #514F4F;font-size:9px">PCode Qty</td><td style="border-bottom:1px solid #514F4F;font-size:9px">PCode Qty</td></tr>
		<?php 
		$totqty = $totamt = 0;
		$cnt=0;
		if(mysql_num_rows($rs1) > 0){		
			while($rows = mysql_fetch_array($rs1)){
					if($cnt==0 || $cnt%4==0) echo '<tr>';
					$qry = "SELECT product_name FROM product WHERE product_id = ".$rows["product_id"];
					$rs2= mysql_query($qry) or print(mysql_error());
					$row2 = mysql_fetch_array($rs2);
					
					echo '<td style="font-size:9px">'.$row2["product_name"].' * '.$rows["quantity"].'</td>';
					//echo "<pre>";print_r($rows);
					$totqty += $rows["quantity"];
					$totamt = $totamt + ($rows["quantity"]*$rows["product_price"]);
					$cnt++;
					if($cnt%4==0) echo '</tr>';
			}
			if($cnt%4!=0){
				$rem=$cnt%4;
				for($i=0;$i<=$rem;$i++){
					echo '<td></td>';
				}
				echo '</tr>';
			}
			?>
            
		<?php	
		}
		?>
        <tr ><td colspan="4" style="border-top:1px solid #514F4F;font-size:9px"><?php echo $bs; ?><span>Product Qty : <?php echo $totqty;?></span>&nbsp;&nbsp;&nbsp;<span>Total Point : <?php echo formatAmt($totamt);?></span><?php echo $be; ?></td></tr>
        <tr ><td align="center" colspan="4" style="border-top:1px solid #514F4F;font-size:9px"><?php if($row["hash_key"] != "" && file_exists("images/codes/".$row["hash_key"].".gif")){ ?>
			<div><img src="<?php echo "images/codes/".$row["hash_key"].".gif"; ?>" /></div>
			<?php } ?></td></tr>
		
		
	</table>
	<?php if($printRequire): ?>
	<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
	<script type="text/javascript">
		<?php if(!isset($_REQUEST["lastrec"])){ ?>
		javascript:window.print();
		$(function () {
			val = 1;
			$( document ).on( 'keydown', function ( e ) {
				if ( e.keyCode === 13 ) { 
					if(val == 1)
						val = val + 1;
					else
					window.location.replace('dashboard.php');
				}
			});
			
			$(document).keydown();
			$(document).keydown();
		});
		<?php }else{ ?>
			javascript:window.print();
			setTimeout("closePrintView()", 10000); //delay required for IE to realise what's going on
	    window.onafterprint = closePrintView(); //this is the thing that makes it work i
	    function closePrintView() { //this function simply runs something you want it to do
	      document.location.href = "dashboard.php"; //in this instance, I'm doing a re-direct
	    }
		<?php } ?>
	</script>
	<?php endif; ?>
	<div class="pagebreak"></div>
</body>
</html>